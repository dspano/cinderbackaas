import os_client_config

from flask_api import FlaskAPI
from flask import request, jsonify
from os_client_config.exceptions import OpenStackConfigException
from uuid import uuid4

# Local imports
from app.backup import BackupService
from app.clouds import CloudData
from app.task import Task
from app.task_service import TaskService
from instance.config import app_config


def create_app(config_name):
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    cloud_data = CloudData(app.config['CLOUD_CONFIG_PATH'])

    @app.route('/api/v1/clouds', methods=['GET'])
    def get_clouds():
        clouds = cloud_data.list_clouds()
        return jsonify(clouds)

    @app.route('/api/v1/clouds/<cloud_name>', methods=['GET'])
    def get_cloud(cloud_name):
        cloud = cloud_data.cloud_detail(cloud_name)
        return jsonify(cloud)

    @app.route('/api/v1/clouds', methods=['POST'])
    def create_cloud():
        cloud_config = request.data
        status, updated_config = cloud_data.create(cloud_config)
        response = jsonify(updated_config)
        response.status_code = status
        return response

    @app.route('/api/v1/backups/<cloud_name>', methods=['GET'])
    def get_backups(cloud_name):
        app.logger.info(f'cloud_name {cloud_name}')
        try:
            cloud_config = os_client_config.OpenStackConfig().get_one_cloud(
                cloud=cloud_name)
            backup_service = BackupService(cloud_config.config)
            backups = backup_service.list()
            return jsonify(backups)
        except os_client_config.exceptions.OpenStackConfigException as e:
            app.logger.error(f'Error loading cloud config {e}')
            return jsonify(f'Error retrieving Openstack cloud settings '
                           f'for {cloud_name}')

    @app.route('/api/v1/backups/<cloud_name>', methods=['POST'])
    def create_backups(cloud_name):
        backup_coverage = request.data
        backup_coverage['cloud'] = cloud_name
        app.logger.info(f'backup_coverage is {backup_coverage}')
        task_service = TaskService()
        locked, locking_uuid = task_service.is_locked('cloud', cloud_name)
        if not locked:
            try:
                # Test to see if the cloud exists otherwise throw an exception
                os_client_config.OpenStackConfig().get_one_cloud(
                    cloud=cloud_name)
                task_uuid = str(uuid4())
                task = Task(task_uuid)
                task_vars = task.save(**backup_coverage)
                task.status(4)
                if task_vars:
                    response = jsonify(task_vars)
                    response.status_code = 201
                    return response
            except OpenStackConfigException as e:
                app.logger.error(f'Error loading cloud config {e}')
                response = jsonify(f'Error retrieving Openstack cloud '
                                   f'settings for {cloud_name}')
                return response
        else:
            msg = f'Project is locked by another backup (uuid: {locking_uuid})'
            app.logger.info(msg)
            response = jsonify(msg)
            response.status_code = 423
            return response

    @app.route('/api/v1/status/<uuid:task_id>', methods=['GET'])
    def get_backup_status(task_id):
        if request.method == 'GET':
            task = Task(task_uuid=str(task_id))
            status_code, status = task.status()
            response = jsonify({
                'status_code': status_code,
                'status': status
            })
            response.status_code = 201
            return response

    return app
