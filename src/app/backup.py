import base64
import json
import logging
import shade
from shade.exc import OpenStackCloudTimeout, OpenStackCloudException

from collections import defaultdict
from operator import attrgetter

format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=format, datefmt='%m-%d %H:%M', level='INFO')
LOG = logging.getLogger(__name__)
# shade.simple_logging(debug=True)


class BackupInfo(object):
    """Representation of volume information to store in backup description."""

    @staticmethod
    def _from_b64_json(data):
        """Convert from base 64 json data to Python objects."""
        return json.loads(base64.b64decode(data))

    def __init__(self, data, object_type=None):
        """Accept string, backup or volume classes."""
        # We're using shade so everything should be a munch.Munch
        if object_type == 'backup':
            self.__dict__ = self._from_b64_json(data.description)

        elif object_type == 'volume':
                self.id = data.id
                self.owner_tenant_id = getattr(data,
                                               'os-vol-tenant-attr:tenant_id')
        # We don't know how to treat additional types
        else:
            raise ValueError('data argument is of unknown '
                             f'object type {object_type}')

    def __repr__(self):
        """Base 64 encodejson representation of instance."""
        return base64.b64encode(json.dumps(self.__dict__).encode()).decode()


class BackupService:

    def __init__(self, cloud_config, timeout=3600):
        self.timeout = timeout
        self.cloud_config = cloud_config
        self.cloud = shade.openstack_cloud(cloud_config=self.cloud_config)
        self.name_prefix = 'auto_backup_'

    def _is_auto_backup(self, backup):
        """Check if a backup was created by us."""

        # It must have the right prefix
        if not backup.name or not backup.name.startswith(self.name_prefix):
            return False

        # And description must contain json formatted data base64 encoded
        try:
            BackupInfo(backup, 'backup')
        except (AttributeError, TypeError):
            return False

        return True

    def _existing_backups(self):
        """Retrieve existing backups and return a defaultdict with backups
        grouped by original volume id."""

        # Get list of backups from Cinder Backup service
        backups = self.cloud.list_volume_backups()

        # Leave only automatic backups based on the backup name
        backups = filter(self._is_auto_backup, backups)

        # Dictionary of volumes with the list of backups for each one
        volumes = defaultdict(list)
        for backup in backups:
            volumes[backup.name[len(self.name_prefix):]].append(backup)

        # Order the backups for each volume oldest first
        for volume in volumes.values():
            volume.sort(key=attrgetter('created_at'))

        return volumes

    def list(self):
        return self._existing_backups()

    def backup_volume(self, volume, name=None):
        """Backup a volume using a volume object or it's id.
        :param volume: Volume object or volume id as a string.
        :param name: Name for the backup
        :param client: If we want ot use a specific client instead of this
                       instance's client. Useful when creating backups for
                       other tenants.
        :return: Backup object
        """
        if isinstance(volume, str):
            # TODO: This could fail
            volume = self.cloud.get_volume(volume)

        # Use given client or instance's client
        name = name or self.name_prefix + volume.id

        # Use encoded original volume info as description
        description = BackupInfo(volume, 'volume')
        LOG.info(f'description is {description} '
                 f'length is {len(str(description))}')
        backup = None
        if volume.status == 'in-use':
            LOG.info('Volume online so this is a multi-step process')

            try:
                # Force snapshot since volume it's in-use
                LOG.info('Creating snapshot')
                snapshot = self.cloud.create_volume_snapshot(
                        volume_id=volume.id, force=True, name='tmp ' + name,
                        description='Temporary snapshot for backup',
                        wait=True, timeout=self.timeout)

                # Create temporary volume from snapshot
                LOG.info('Creating temporary volume from snapshot')
                tmp_vol = self.cloud.create_volume(
                        size=snapshot.size, snapshot_id=snapshot.id,
                        name='tmp '+name,
                        description='Temporary volume for backup',
                        wait=True, timeout=self.timeout)

                # Backup temporary volume
                LOG.info('Performing actual backup of volume')
                backup = self.cloud.create_volume_backup(
                        volume_id=tmp_vol.id, name=name,
                        description=str(description),
                        wait=True, timeout=self.timeout)

            except (OpenStackCloudException,
                    OpenStackCloudTimeout) as e:
                LOG.error(f'Backup creation of {volume.id} '
                          f'failed with error {e}')
                raise

            # Cleanup temporary resources
            LOG.info('Deleting temporary volume and snapshot')
            self.cloud.delete_volume(tmp_vol.id)
            self.cloud.delete_volume_snapshot(snapshot.id)
            LOG.info('Cleanup complete')

        elif volume.status == 'available':
            try:
                LOG.info('Status is available creating direct backup')
                backup = self.cloud.create_volume_backup(
                    volume_id=volume.id, name=name,
                    description=str(description), force=False,
                    wait=True, timeout=self.timeout)

            except (OpenStackCloudException,
                    OpenStackCloudTimeout) as e:
                LOG.error(f'Backup creation of {volume.id} '
                          f'failed with error {e}')
                raise

        return backup

    def backup_all(self, keep_only=0):
        """Creates backup for all visible volumes.

        :param keep_only: Amount of backups to keep including the new one.
                          Older ones will be deleted.
        :return: ([successful_backup_object], [failed_volume_object])
        """

        backups = []
        failed = []

        # Get visible volumes
        volumes = self.cloud.list_volumes()

        # Get existing backups
        existing_backups = self._existing_backups()

        LOG.info('Starting Volumes Backup')
        for vol in volumes:
            LOG.info(f'Processing {vol.size}GB from volume {vol.name} '
                     f'(id: {vol.id})')
            backup_name = self.name_prefix + vol.id

            # Do the backup
            try:
                backup = self.backup_volume(vol, name=backup_name)
            except OpenStackCloudTimeout:
                LOG.error('Timeout on backup')
                failed.append(vol)
                backup = None
            except OpenStackCloudException as e:
                LOG.error(f'Error while doing backup {e}')
                failed.append(vol)
                break
            except Exception as e:
                LOG.error(f'Unexpected Exception while doing backup {e}')
                failed.append(vol)
                backup = None

            # On success
            else:
                backups.append(backup)
                existing_backups[vol.id].append(backup)
                # If we limit the number of backups and we have too many
                # backups for this volume
                if (keep_only and len(existing_backups.get(vol.id, tuple())) >
                        keep_only):
                    remove = len(existing_backups[vol.id]) - keep_only
                    # We may have to remove multiple backups and we remove the
                    # oldest ones, which are the first on the list.
                    for __ in range(remove):
                        back = existing_backups[vol.id].pop(0)
                        LOG.info('Removing old backup %s from %s', back.id,
                                 back.created_at)
                        self.cloud.delete_volume_backup(back.id)
            LOG.info('Backup completed')
        LOG.info('Finished with backups')
        return backups, failed
