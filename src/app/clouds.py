import logging
import os
import ruamel.yaml as yaml

from pathlib import Path

format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=format, datefmt='%m-%d %H:%M', level='INFO')
LOG = logging.getLogger(__name__)


class CloudData:

    def __init__(self, path):
        self.path = path

    def _get_cloud_data(self):

        if os.path.exists(self.path):
            with open(self.path, 'r') as cloud_config:
                return yaml.safe_load(cloud_config)

        return None

    def _update_cloud_data(self, new_cloud):

        cloud_data = self._get_cloud_data()
        if not cloud_data:
            cloud_data = {'clouds': ''}

        updated_data = dict(cloud_data['clouds'], **new_cloud['clouds'])
        cloud_data['clouds'] = updated_data

        return cloud_data

    def list_clouds(self):

        clouds = []
        cloud_config = self._get_cloud_data()

        if cloud_config:
            clouds = [*cloud_config['clouds'].keys()]
        return clouds

    def cloud_detail(self, cloud_name):
        """

        :param cloud_name:
        :return:
        """
        cloud_data = {}
        cloud_config = self._get_cloud_data()
        if cloud_config:
            cloud_data = cloud_config['clouds'][cloud_name]['auth']
            cloud_data['password'] = 'redacted'

        return cloud_data

    def create(self, new_cloud):
        """

        :param new_cloud: New cloud data to be added
        :return: response, redacted cloud data
        """
        try:
            updated_clouds = self._update_cloud_data(new_cloud)
            new_config = yaml.YAML(typ='safe')
            new_config.default_flow_style = False
            cloud_config_file = Path(self.path)
            new_config.dump(updated_clouds, cloud_config_file)

            return 201, new_cloud
        except Exception as e:
            msg = f'New config creation failed {e}'
            LOG.error(msg)
            return 500, msg
