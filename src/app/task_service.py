import logging
import os
import os_client_config

from os_client_config.exceptions import OpenStackConfigException

from app.constants import TASK_DB_PATH
from app.task import Task
from app.backup import BackupService

format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=format, datefmt='%m-%d %H:%M', level='INFO')
LOG = logging.getLogger(__name__)


class TaskService:

    def __init__(self):
        self.received_tasks = []
        if not os.path.exists(TASK_DB_PATH):
            os.mkdir(TASK_DB_PATH)

    def _get_active_tasks(self):
        """
        Grab a list of task databases, and check each one to see if their
        status is within the range considered active 2-4
        """

        tasks = []

        for filename in os.listdir(TASK_DB_PATH):
            path = os.path.join(TASK_DB_PATH, filename)
            if os.path.exists(path):
                task_uuid = filename.split('.')[0]
                task = Task(task_uuid)
                task_dict = task.get()

                if task_dict.get('status') and task_dict.get('status') > 1:
                    LOG.debug(f'task is {task_dict}')
                    tasks.append(task_dict)

        return tasks

    def is_locked(self, lock_key, lock_value):
        """
        Grab the list of active tasks, then check to see if their dictionaries
        contain the lock key and value we specify to determin if it's locked
        or not.

        :param lock_key: string corresponding to the key to check
        :param lock_value: string corresponding with the value that determines
                           if the task is locked
        :return: bool, task uuid/None
        """
        tasks = self._get_active_tasks()

        for task in tasks:
            if task and task.get(lock_key) == lock_value:
                return True, task['uuid']

        return False, None

    def get_received_tasks(self):
        """
        Get the list of active tasks, and set the received_tasks
        attribute to the subset of tasks that are received(status 4)
        """

        tasks = self._get_active_tasks()
        LOG.info('Checking for received tasks')
        self.received_tasks = [task for task in tasks
                               if task['status'] == 4]

    def start_backups(self):
        """
        Iterate the list of received tasks, and if they have the correct
        coverage value set, start the backup for that task
        """

        LOG.debug('Starting backup run')
        for task in self.received_tasks:
            try:
                if task:
                    updated_task = Task(task['uuid'])
                    updated_task.status(3)
                    LOG.info('Starting backup for {0}'.format(task['uuid']))
                    cloud_config = os_client_config.OpenStackConfig().get_one_cloud( # noqa
                        cloud=task['cloud'])
                    backup_service = BackupService(cloud_config.config)
                    if task['coverage'] == 'all':
                        LOG.info('Backing up all volumes')
                        updated_task.status(2)
                        backups, failed = backup_service.backup_all(
                            keep_only=1)
                        task['completed'] = backups
                        task['failed'] = failed
                        updated_task = Task(task['uuid'])
                        updated_task.save(**task)
                        updated_task.status(0)
                        LOG.info('Full backup completed')
            except OpenStackConfigException as e:
                LOG.error(f'Error loading cloud config {e}')
                updated_task = Task(task['uuid'])
                updated_task.status(1)

            except Exception as e:
                LOG.error('Backup failed with error {0}'.format(e))
                updated_task = Task(task['uuid'])
                updated_task.status(1)

        LOG.debug('Backup run complete')
