import logging
import os
import shelve

from app.constants import STATUS, TASK_DB_PATH

format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(format=format, datefmt='%m-%d %H:%M', level='INFO')
LOG = logging.getLogger(__name__)


class Task:
    def __init__(self, task_uuid):
        self.task_uuid = task_uuid
        path = '{0}'.format(TASK_DB_PATH)
        if not os.path.exists(path):
            os.makedirs(path)
        self.db_path = '{0}/{1}.db'.format(path, self.task_uuid)

    def save(self, **kwargs):
        """
        Takes in kwargs and saves to a current task's database

        :param kwargs: dictionary of task data
        :return task: updated task dictionary
        """

        with shelve.open(self.db_path) as task_db:

            if self.task_uuid is not None:
                kwargs['uuid'] = self.task_uuid

                for k, v in kwargs.items():
                    task_db[k] = v

                task = dict(task_db)

                return task

    def get(self):
        """
        Retrieve current tasks data from it's task db

        :return task: dictionary of task data
        """

        with shelve.open(self.db_path, flag='r') as task_db:
            task = dict(task_db)
            return task

    def status(self, status=None):
        """
        An idempotent method that creates or retrieves a task status from
        a shelf database file.
        :param task_uuid:
        :param status:
        :return:
        """

        if status is not None:
            LOG.info('Setting task {0} status to {1}'.format(self.task_uuid,
                                                             status))
            with shelve.open(self.db_path) as task_db:
                task_db['status'] = status
        else:
            with shelve.open(self.db_path, flag='r') as task_db:
                status = task_db['status']

        return status, STATUS[status]
