STATUS = {0: 'completed', 1: 'error', 2: 'started', 3: 'pending',
          4: 'received'}
TASK_DB_PATH = '/tmp/backup_status'
TASK_POLLER_INTERVAL = 30
