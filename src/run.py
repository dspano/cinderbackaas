import os

# Local imports
import poll_tasks
from app import create_app

config_name = os.getenv('APP_SETTINGS')  # config_name = "development"
app = create_app(config_name)

if __name__ == '__main__':
    task_service = Process(target=poll_tasks.start_service)
    task_service.start()
    app.run()
