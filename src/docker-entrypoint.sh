#!/usr/bin/env bash

if [ "$1" == "run" ]; then
  python poll_tasks.py &
  gunicorn run:app --bind 0.0.0.0:5000 --timeout=6000 --workers=$API_WORKERS
elif [ "$1" == "test" ]; then
  tox
else
  exec $@
fi

