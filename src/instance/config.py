import os


class Config(object):
    """Parent configuration class."""
    DEBUG = False
    CSRF_ENABLED = True
    SECRET = os.getenv('SECRET')
    CLOUD_CONFIG_PATH = '/etc/openstack/clouds.yaml'
    SHADE_TIMEOUT = 3600

class DevelopmentConfig(Config):
    """Configurations for Development."""

    DEBUG = True
    CLOUD_CONFIG_PATH = './tests/files/clouds.yaml'
    SHADE_TIMEOUT = 1

class TestingConfig(Config):
    """Configurations for Testing."""

    DEBUG = True
    TESTING = True

class ProductionConfig(Config):
    """Configurations for Production."""

    DEBUG = False
    TESTING = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig
}
