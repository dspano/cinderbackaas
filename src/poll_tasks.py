import logging
import os
from multiprocessing import Pool
from time import sleep

from app.task_service import TaskService
from app.constants import TASK_POLLER_INTERVAL

LOG = logging.getLogger(__name__)

TASK_SERVICE_WORKERS = os.environ.get('TASK_SERVICE_WORKERS', 4)


def start_service():

    while True:
        try:
            task_service = TaskService()
            task_service.get_received_tasks()
            task_service.start_backups()
            sleep(TASK_POLLER_INTERVAL)

        except Exception as e:
            LOG.error('Exception {0}'.format(e))
            sleep(TASK_POLLER_INTERVAL)


if __name__ == '__main__':
    with Pool(processes=4) as pool:
        start_service()
