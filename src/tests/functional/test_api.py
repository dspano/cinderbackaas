import json
import munch
import os
import ruamel.yaml as yaml
import shutil

from os_client_config.exceptions import OpenStackConfigException
from unittest import mock

# Local imports
from app import create_app
from app.constants import TASK_DB_PATH
from app.task import Task


class TestAPI(object):

    def setup_method(self):
        self.app = create_app(config_name="development")
        self.client = self.app.test_client
        self.response = mock.Mock()
        self.test_config_path = os.path.join(os.getcwd(),
                                             'tests/files/clouds.yaml')
        shutil.rmtree(TASK_DB_PATH, ignore_errors=True)

    @mock.patch('app.clouds.yaml')
    def test_get_clouds(self, mock_cloud_config):
        clouds = ['mycloud', 'mycloud2']

        with open(self.test_config_path, 'r') as cloud_config:
            mock_cloud_config.safe_load.return_value = yaml.safe_load(
                cloud_config)

        app_response = self.client().get('/api/v1/clouds')
        app_response_dict = json.loads(app_response.data)

        assert app_response_dict == clouds
        assert app_response.status_code == 200

    @mock.patch('app.clouds.yaml')
    def test_get_cloud_details(self, mock_cloud_config):
        cloud_data = {'username': 'myusername',
                      'password': 'redacted',
                      'project_name': 'cloud-project',
                      'auth_url': 'https://localhost:13000/v3',
                      'user_domain_name': 'mydomain'}

        with open(self.test_config_path, 'r') as cloud_config:
            mock_cloud_config.safe_load.return_value = yaml.safe_load(
                cloud_config)

        app_response = self.client().get('/api/v1/clouds/mycloud')
        app_response_dict = json.loads(app_response.data)

        assert app_response_dict == cloud_data
        assert app_response.status_code == 200

    def test_add_cloud_to_config(self):
        post_data = {
            'clouds': {
                'mycloud2': {
                    'auth': {'username': 'newuser',
                             'password': 'password123',
                             'project_name': 'cloud-project',
                             'auth_url': 'https://localhost:13000/v3',
                             'user_domain_name': 'mydomain'}
                }
            }
        }

        app_response = self.client().post('/api/v1/clouds',
                                          data=json.dumps(post_data),
                                          content_type='application/json')

        assert app_response.status_code == 201

    @mock.patch('app.backup.shade')
    @mock.patch('app.os_client_config')
    def test_get_cloud_backups(self, mock_os_client_config, mock_shade):

        backup_data = {'a53bc28a-8976-497e-ac18-3b9bc2b1a658': [
            {
                'status': 'available', 'object_count': 0,
                'container': 'backups',
                'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
                'links': [
                    {'href': 'https://svs-rtp-auto-ocloud-1.cisco.com:13776'
                             '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                             '8fac285e-9608-49a5-90bb-2450d7eff152',
                     'rel': 'self'},
                    {'href': 'https://svs-rtp-auto-ocloud-1.cisco.com:13776'
                             '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                             '8fac285e-9608-49a5-90bb-2450d7eff152',
                     'rel': 'bookmark'}
                ],
                'availability_zone': 'nova',
                'created_at': '2018-04-03T03:41:12.000000',
                'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                               'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                               'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                               'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
                'updated_at': '2018-04-03T03:43:13.000000',
                'data_timestamp': '2018-04-03T03:41:12.000000',
                'has_dependent_backups': False,
                'snapshot_id': None,
                'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
                'fail_reason': None, 'is_incremental': False,
                'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50},
            {
                'status': 'available',
                'object_count': 0, 'container': 'backups',
                'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
                'links': [
                    {'href': 'https://svs-rtp-auto-ocloud-1.cisco.com:13776'
                             '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                             '6c7c35e5-6609-4842-b1ff-db36114f00e1',
                     'rel': 'self'},
                    {'href': 'https://svs-rtp-auto-ocloud-1.cisco.com:13776'
                             '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                             '6c7c35e5-6609-4842-b1ff-db36114f00e1',
                     'rel': 'bookmark'}
                ],
                'availability_zone': 'nova',
                'created_at': '2018-04-03T03:43:50.000000',
                'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                               'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                               'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                               'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
                'updated_at': '2018-04-03T03:45:51.000000',
                'data_timestamp': '2018-04-03T03:43:50.000000',
                'has_dependent_backups': False, 'snapshot_id': None,
                'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
                'fail_reason': None, 'is_incremental': False,
                'id': '6c7c35e5-6609-4842-b1ff-db36114f00e1', 'size': 50}
        ]
        }
        mock_backups = []
        for backup_list in backup_data.values():
            for backup in backup_list:
                munch_munch = munch.Munch(backup)
                mock_backups.append(munch_munch)
        mock_shade.openstack_cloud().list_volume_backups.return_value = mock_backups
        app_response = self.client().get('/api/v1/backups/mycloud')
        app_response_dict = json.loads(app_response.data)

        assert app_response_dict == backup_data
        assert app_response.status_code == 200

    def test_get_cloud_backup_negative(self):
        """
        Ensure we receive a failure message if we try to grab a non-existent cloud
        """

        app_response = self.client().get('/api/v1/backups/dontexist')
        app_response_error = json.loads(app_response.data)
        assert 'Error retrieving Openstack cloud settings' in app_response_error

    def test_get_task_status(self):
        """
        Ensure we receive the proper status response when the status endppoint
        is checked
        """

        t = Task('d72cbd8f-412c-409f-ae4b-36441e172922')
        t.status(2)
        app_response = self.client().get('/api/v1/status/d72cbd8f-412c-409f-ae4b-36441e172922')
        app_response_dict = json.loads(app_response.data)
        assert app_response_dict['status_code'] == 2
        assert app_response_dict['status'] == 'started'

    @mock.patch('app.os_client_config')
    @mock.patch('app.TaskService')
    def test_create_backups(self, mock_task_service, mock_os_client_config):
        """
        Ensure we are able to create a task object and return the right
        response when a POST to backups/<cloud_name> is sent
        """


        post_data = {'coverage': 'donkey'}
        mock_task_service().is_locked.return_value = False, None

        app_response = self.client().post('/api/v1/backups/mycloud',
                                          data=json.dumps(post_data),
                                          content_type='application/json')
        app_response_dict = json.loads(app_response.data)
        print(app_response_dict)
        assert app_response.status_code == 201
        assert app_response_dict['cloud'] == 'mycloud'
        assert app_response_dict['coverage'] == 'donkey'
        assert app_response_dict['uuid'] is not None

    @mock.patch('app.TaskService')
    def test_create_backups_is_locked(self, mock_task_service):
        """
        Ensure we are able to create a task object and return the right
        response when a POST to backups/<cloud_name> is sent
        """


        post_data = {'coverage': 'donkey'}

        mock_task_service().is_locked.return_value = True, 'abc1234'

        app_response = self.client().post('/api/v1/backups/mycloud',
                                          data=json.dumps(post_data),
                                          content_type='application/json')

        app_response_dict = json.loads(app_response.data)

        assert 'Project is locked by another backup' in app_response_dict

    @mock.patch('app.os_client_config')
    @mock.patch('app.TaskService')
    def test_create_backups_cloud_config_exception(self, mock_task_service,
                                                   mock_os_client_config):
        """
        Ensure we are able to create a task object and return the right
        response when a POST to backups/<cloud_name> is sent
        """

        def mock_get_one_cloud(cloud):
            raise OpenStackConfigException('Something bad happened!')

        post_data = {'coverage': 'donkey'}
        mock_task_service().is_locked.return_value = False, None
        mock_os_client_config.OpenStackConfig().get_one_cloud.side_effect = mock_get_one_cloud
        app_response = self.client().post('/api/v1/backups/mycloud',
                                          data=json.dumps(post_data),
                                          content_type='application/json')
        app_response_error = json.loads(app_response.data)

        assert 'Error retrieving Openstack cloud settings' in app_response_error
