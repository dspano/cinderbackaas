import os
import shutil

from os_client_config.exceptions import OpenStackConfigException
from unittest import mock
from uuid import uuid4

from app.constants import TASK_DB_PATH
from app.task import Task
from app.task_service import TaskService


class TestTaskService(object):

    def setup_method(self):
        """
        Cleanup the status directory after each test
        """

        shutil.rmtree(TASK_DB_PATH, ignore_errors=True)

    def test_get_received_tasks(self):
        """
        Ensure task_service returns received tasks
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_get',
                     'cloud': 'mycloud'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(4)
        ts = TaskService()
        ts.get_received_tasks()

        assert task_uuid in ts.received_tasks[0]['uuid']

    def test_get_received_empty_when_status_not_received(self):
        """
        Ensure task_service returns an empty list when there are no
        received tasks
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_get',
                     'cloud': 'mycloud'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(1)
        ts = TaskService()
        ts.get_received_tasks()

        assert ts.received_tasks == []

    def test_is_locked(self):
        """
        Ensure task_service reports is_locked is true if at least
        one task is active
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_get',
                     'cloud': 'mycloud'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(4)
        ts = TaskService()
        locked, locking_uuid = ts.is_locked('cloud', 'mycloud')

        assert locked
        assert locking_uuid == task_uuid

    def test_is_locked_not_active(self):
        """
        Ensure task_service reports false and None for locked and
        locking uuid if there are no active tasks
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_get',
                     'cloud': 'mycloud'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(0)
        ts = TaskService()
        locked, locking_uuid = ts.is_locked('cloud', 'mycloud')

        assert not locked
        assert locking_uuid is None

    def test_task_service_creates_db_path(self):
        """
        Ensure TaskService creates the db path upon instantiation
        """

        shutil.rmtree(TASK_DB_PATH, ignore_errors=True)
        _ = TaskService()
        assert os.path.exists(TASK_DB_PATH)

    @mock.patch('app.backup.shade')
    @mock.patch('app.task_service.os_client_config')
    @mock.patch('app.backup.BackupService')
    def test_start_backup(self, mock_backup_service, mock_os_client_config,
                          mock_shade):
        """
        Ensure task status is set to completed when a backup
        completes without an exception.
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'all',
                     'cloud': 'mycloud'}
        mock_backup_service.backup_all.return_value = [1, 0]
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(4)
        ts = TaskService()
        ts.get_received_tasks()
        ts.start_backups()
        task_vars = t.get()

        assert task_vars['status'] == 0

    @mock.patch('app.task_service.os_client_config')
    @mock.patch('app.backup.BackupService')
    def test_start_backup_cloud_config_exception(self, mock_backup_service,
                                                 mock_os_client_config):
        """
        Ensure task status is set to completed when a backup
        completes with an OpenstackConfigException.
        """

        def mock_get_one_cloud(cloud):
            raise OpenStackConfigException('Something bad happened!')

        task_uuid = str(uuid4())
        task_data = {'coverage': 'all',
                     'cloud': 'mycloud'}
        mock_os_client_config.OpenStackConfig().get_one_cloud.side_effect = mock_get_one_cloud
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(4)
        ts = TaskService()
        ts.get_received_tasks()
        ts.start_backups()
        task_vars = t.get()

        assert task_vars['status'] == 1

    @mock.patch('app.task_service.os_client_config')
    @mock.patch('app.backup.BackupService')
    def test_start_backup_cloud_general_exception(self, mock_backup_service,
                                                  mock_os_client_config):
        """
        Ensure task status is set to error when a backup
        completes with a general exception.
        """

        def mock_get_one_cloud(cloud):
            raise Exception('Something bad happened!')

        task_uuid = str(uuid4())
        task_data = {'coverage': 'all',
                     'cloud': 'mycloud'}
        mock_os_client_config.OpenStackConfig().get_one_cloud.side_effect = mock_get_one_cloud
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        t = Task(task_uuid)
        t.save(**task_data)
        t.status(4)
        ts = TaskService()
        ts.get_received_tasks()
        ts.start_backups()
        task_vars = t.get()

        assert task_vars['status'] == 1
