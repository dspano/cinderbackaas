import base64
import json
import munch
import pytest

from app.backup import BackupInfo


class TestBackupInfo(object):

    def test_backup_info_b64_to_json(self):
        """Ensure we're able to decode volume metadata stored in the backup
        description"""

        data_dict = {
            'description': 'eyJpZCI6ICJiMzcwZWE1OC00MWJlLTQ3YTYtYTFlOC0zNm'
                           'UyYWYzNDViZDQiLCAib3duZXJfdGVuYW50X2lkIjogIjdiO'
                           'GY5YjcxZTg2NDQyYTJiZjgzYTYzZDNhYjViM2U0In0='}
        backup_data = {'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
                       'owner_tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4'}

        data = munch.Munch(data_dict)
        b = BackupInfo(data, 'backup')

        assert b._from_b64_json(data.description) == backup_data

    def test_backup_info_encode_to_b64(self):
        """Ensure we're able to encode volume metadata stored in the backup
        description and get the right dictionary when decoded."""

        volume_data = {'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
                       'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4'}

        decoded_backup_data = {'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
                               'owner_tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4'}

        data = munch.Munch(volume_data)
        b = BackupInfo(data, 'volume')
        decoded = json.loads(base64.b64decode(str(b)))
        assert decoded == decoded_backup_data

    def test_backup_info_wrong_object_type(self):
        """Test that a ValueError is raised when the wrong object_type
        is used. Should be 'backup' or 'volume'
        """

        data_dict = {
            'description': 'eyJpZCI6ICJiMzcwZWE1OC00MWJlLTQ3YTYtYTFlOC0zNm'
                           'UyYWYzNDViZDQiLCAib3duZXJfdGVuYW50X2lkIjogIjdi'
                           'OGY5YjcxZTg2NDQyYTJiZjgzYTYzZDNhYjViM2U0In0='}
        data = munch.Munch(data_dict)
        with pytest.raises(ValueError) as excinfo:
            b = BackupInfo(data, 'conway_twitty')
