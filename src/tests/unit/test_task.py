import shutil

from uuid import uuid4

from app.constants import TASK_DB_PATH
from app.task import Task


class TestTasks(object):

    def setup_method(self):
        """
        Cleanup the status directory after each test
        """

        shutil.rmtree(TASK_DB_PATH, ignore_errors=True)

    def test_save_task(self):
        """
        Ensure tasks can be saved and task_vars is correct
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'all'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        task = Task(task_uuid)
        task_vars = task.save(**task_data)

        assert task_vars == task_return_data

    def test_get_task(self):
        """
        Ensure we can get task data
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_get'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        task = Task(task_uuid)
        task.save(**task_data)
        task_vars = task.get()

        assert task_vars == task_return_data

    def test_task_get_status_when_params_not_none(self):
        """
        Ensure we can set status and receive a status
        when a status code is passed in.
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_status'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        task = Task(task_uuid)
        task.save(**task_data)
        status_code, status = task.status(2)

        assert status == 'started'

    def test_task_get_status_when_params_none(self):
        """
        Ensure the status method returns the current status
        when no params are passed in.
        """

        task_uuid = str(uuid4())
        task_data = {'coverage': 'test_status'}
        task_return_data = {'uuid': task_uuid}
        task_return_data.update(task_data)
        task = Task(task_uuid)
        task.save(**task_data)
        task.status(4)
        status_code, status = task.status()

        assert status == 'received'