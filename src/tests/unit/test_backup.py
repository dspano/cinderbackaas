import munch
import pytest
from shade.exc import OpenStackCloudTimeout, OpenStackCloudException
from unittest import mock

from app.backup import BackupService


class TestBackupService(object):

    @mock.patch('app.backup.shade')
    def test_is_auto_backup(self, mock_shade):
        """Ensure _is_auto_backup returns true when a backup munch object
        is passed in"""

        backup = {
            'status': 'available', 'object_count': 0,
            'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}

        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config)
        backup_munch = munch.Munch(backup)
        assert backup_service._is_auto_backup(backup_munch)

    @mock.patch('app.backup.shade')
    def test_is_auto_backupis_false_when_name_is_wrong(self, mock_shade):
        """Ensure _is_auto_backup returns true when a backup munch object
        is passed in"""

        backup = {
            'status': 'available', 'object_count': 0,
            'name': 'dwight_shrute_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}

        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config)
        backup_munch = munch.Munch(backup)
        assert not backup_service._is_auto_backup(backup_munch)

    @mock.patch('app.backup.shade')
    def test_is_auto_backup_false_on_attribute_error(self, mock_shade):
        """Ensure _is_auto_backup returns true when a backup munch object
        is passed in. This should fail because it doesn't have
        the right attributes. We need name, id, description and
        os-vol-tenant-attr:tenant_id as attribures"""

        backup = {
            'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}

        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config)
        backup_munch = munch.Munch(backup)
        assert not backup_service._is_auto_backup(backup_munch)

    @mock.patch('app.backup.shade')
    def test_backup_list(self, mock_shade):
        """
        Ensure we return the right list of backup data from shade.
        """

        backup1 = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'fail_reason': None, 'is_incremental': False,
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}
        backup2 = {
            'status': 'available',
            'object_count': 0, 'container': 'backups',
            'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '6c7c35e5-6609-4842-b1ff-db36114f00e1',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '6c7c35e5-6609-4842-b1ff-db36114f00e1',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:43:50.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:45:51.000000',
            'data_timestamp': '2018-04-03T03:43:50.000000',
            'has_dependent_backups': False, 'snapshot_id': None,
            'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'fail_reason': None, 'is_incremental': False,
            'id': '6c7c35e5-6609-4842-b1ff-db36114f00e1', 'size': 50}
        backup_data = {'a53bc28a-8976-497e-ac18-3b9bc2b1a658': ''}
        mock_backups = []
        # Convert backup data to Munch objects
        for backup in [backup1, backup2]:
            munch_munch = munch.Munch(backup)
            mock_backups.append(munch_munch)

        mock_shade.openstack_cloud().list_volume_backups.return_value = mock_backups
        backup_data['a53bc28a-8976-497e-ac18-3b9bc2b1a658'] = mock_backups
        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config)
        backups = backup_service.list()
        assert backups == backup_data

    @mock.patch('app.backup.shade')
    def test_create_backup(self, mock_shade):
        """
        Ensure we can create a regular backup with a volume object
        """

        # Create a dictionary representation of the subset of volume metadata
        # we need
        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'in-use',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        snapshot = {
            'status': 'available',
            'metadata': {
                'name': 'test'
            },
            'os-extended-snapshot-attributes:progress': '100%',
            'name': 'test-volume-snapshot',
            'volume_id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'os-extended-snapshot-attributes:project_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'created_at': '2018-04-05T18:30:01.000000',
            'size': 50,
            'id': 'b1323cda-8e4b-41c1-afc5-2fc791809c8c',
            'description': 'volume snapshot'
        }
        temp_volume = {
            'id': '3a4c8cdc-3910-11e8-9280-c85b76e35b22',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'in-use',
            'created_at': '2018-04-05T18:29:33.000000',
            'updated_at': '2018-04-05T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}

        volume_backup = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJiMzcwZWE1OC00MWJlLTQ3YTYtYTFlOC0zNm'
                           'UyYWYzNDViZDQiLCAib3duZXJfdGVuYW50X2lkIjogIjdi'
                           'OGY5YjcxZTg2NDQyYTJiZjgzYTYzZDNhYjViM2U0In0=',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'fail_reason': None, 'is_incremental': False,
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}

        mock_shade.openstack_cloud().list_volume_backups.return_value = mock.Mock()
        mock_shade.openstack_cloud().create_volume_snapshot.return_value = munch.Munch(snapshot)
        mock_shade.openstack_cloud().create_volume.return_value = munch.Munch(temp_volume)
        mock_shade.openstack_cloud().create_volume_backup.return_value = munch.Munch(volume_backup)
        mock_shade.openstack_cloud().delete_volume.return_value = mock.Mock()
        mock_shade.openstack_cloud().delete_volume_snapshot.return_value = mock.Mock()
        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config, 30)
        volume = munch.Munch(backup_volume)
        backup_service.backup_volume(volume)
        mock_shade.openstack_cloud().create_volume_backup.assert_called_with(
            description='eyJpZCI6ICJiMzcwZWE1OC00MWJlLTQ3YTYtYTFlOC0zNmUyY'
                        'WYzNDViZDQiLCAib3duZXJfdGVuYW50X2lkIjogIjdiOGY5Yj'
                        'cxZTg2NDQyYTJiZjgzYTYzZDNhYjViM2U0In0=',
            name='auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            volume_id='3a4c8cdc-3910-11e8-9280-c85b76e35b22',
            wait=True, timeout=30
        )

    @mock.patch('app.backup.shade')
    def test_create_backup_by_uuid(self, mock_shade):
        """
        Ensure we can create a backup with a uuid string
        """

        # Create a dictionary representation of the subset of volume metadata
        # we need
        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'in-use',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        snapshot = {
            'status': 'available',
            'metadata': {
                'name': 'test'
            },
            'os-extended-snapshot-attributes:progress': '100%',
            'name': 'test-volume-snapshot',
            'volume_id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'os-extended-snapshot-attributes:project_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'created_at': '2018-04-05T18:30:01.000000',
            'size': 50,
            'id': 'b1323cda-8e4b-41c1-afc5-2fc791809c8c',
            'description': 'volume snapshot'
        }
        temp_volume = {
            'id': '3a4c8cdc-3910-11e8-9280-c85b76e35b22',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'in-use',
            'created_at': '2018-04-05T18:29:33.000000',
            'updated_at': '2018-04-05T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}

        volume_backup = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJiMzcwZWE1OC00MWJlLTQ3YTYtYTFlOC0zNm'
                           'UyYWYzNDViZDQiLCAib3duZXJfdGVuYW50X2lkIjogIjdi'
                           'OGY5YjcxZTg2NDQyYTJiZjgzYTYzZDNhYjViM2U0In0=',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'fail_reason': None, 'is_incremental': False,
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}
        volume_id = 'b370ea58-41be-47a6-a1e8-36e2af345bd4'
        mock_shade.openstack_cloud().get_volume.return_value = munch.Munch(backup_volume)
        mock_shade.openstack_cloud().list_volume_backups.return_value = mock.Mock()
        mock_shade.openstack_cloud().create_volume_snapshot.return_value = munch.Munch(snapshot)
        mock_shade.openstack_cloud().create_volume.return_value = munch.Munch(temp_volume)
        mock_shade.openstack_cloud().create_volume_backup.return_value = munch.Munch(volume_backup)
        mock_shade.openstack_cloud().delete_volume.return_value = mock.Mock()
        mock_shade.openstack_cloud().delete_volume_snapshot.return_value = mock.Mock()
        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config, 30)
        backup_service.backup_volume(volume_id)
        mock_shade.openstack_cloud().create_volume_backup.assert_called_with(
            description='eyJpZCI6ICJiMzcwZWE1OC00MWJlLTQ3YTYtYTFlOC0zNmUyY'
                        'WYzNDViZDQiLCAib3duZXJfdGVuYW50X2lkIjogIjdiOGY5Yj'
                        'cxZTg2NDQyYTJiZjgzYTYzZDNhYjViM2U0In0=',
            name='auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            volume_id='3a4c8cdc-3910-11e8-9280-c85b76e35b22',
            wait=True, timeout=30
        )

    @mock.patch('app.backup.shade')
    def test_create_backup_catches_cloud_timeout(self, mock_shade):
        """
        Ensure we return none gracefully when an OpenstackCloudTimeout
        exception is thrown.
        """

        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'in-use',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        def mock_shade_exception(volume_id, force, name, description, wait,
                                 timeout):
            raise OpenStackCloudTimeout('timed out!')

        cloud_config = mock.MagicMock()
        volume = munch.Munch(backup_volume)
        mock_shade.openstack_cloud().create_volume_snapshot.side_effect = mock_shade_exception
        mock_shade.openstack_cloud().delete_volume.return_value = mock.Mock()
        mock_shade.openstack_cloud().delete_volume_snapshot.return_value = mock.Mock()
        with pytest.raises(OpenStackCloudTimeout):
            backup_service = BackupService(cloud_config.config, 30)
            backup = backup_service.backup_volume(volume)
            assert backup is None

        volume2 = munch.Munch(backup_volume)
        volume2.status = 'available'
        mock_shade.openstack_cloud().create_volume_backup.side_effect = mock_shade_exception
        with pytest.raises(OpenStackCloudTimeout):
            backup_service = BackupService(cloud_config.config, 30)
            backup = backup_service.backup_volume(volume2)
            assert backup is None

    @mock.patch('app.backup.shade')
    def test_create_backup_catches_cloud_exception(self, mock_shade):
        """
        Ensure we return none gracefully when an OpenstackCloudException
         is thrown.
        """
        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'in-use',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        def mock_shade_exception(volume_id, force, name, description, wait,
                                 timeout):
            raise OpenStackCloudException('timed out!')

        cloud_config = mock.MagicMock()
        volume = munch.Munch(backup_volume)
        mock_shade.openstack_cloud().create_volume_snapshot.side_effect = mock_shade_exception
        mock_shade.openstack_cloud().delete_volume.return_value = mock.Mock()
        mock_shade.openstack_cloud().delete_volume_snapshot.return_value = mock.Mock()
        with pytest.raises(OpenStackCloudException):
            backup_service = BackupService(cloud_config.config, 30)
            backup = backup_service.backup_volume(volume)
            assert backup is None

        volume2 = munch.Munch(backup_volume)
        volume2.status = 'available'
        mock_shade.openstack_cloud().create_volume_backup.side_effect = mock_shade_exception
        with pytest.raises(OpenStackCloudException):
            backup_service = BackupService(cloud_config.config, 30)
            backup = backup_service.backup_volume(volume2)
            assert backup is None

    @mock.patch('app.backup.shade')
    def test_backup_all(self, mock_shade):
        """
        Given one backup volume and two volume backups, ensure that backup_all
        completes properly, and removes 1 backup when keep_only=1
        """

        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'available',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        volume_backup1 = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'fail_reason': None, 'is_incremental': False,
            'id': 'e8457649-671c-4646-8a8a-5aaf39916e95', 'size': 50}
        volume_backup2 = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'a53bc28a-8976-497e-ac18-3b9bc2b1a658',
            'fail_reason': None, 'is_incremental': False,
            'id': '8fac285e-9608-49a5-90bb-2450d7eff152', 'size': 50}
        backup_volumes = [munch.Munch(backup_volume)]
        backups = [munch.Munch(backup)
                   for backup in [volume_backup1, volume_backup2]]
        mock_shade.openstack_cloud().list_volumes.return_value = backup_volumes
        mock_shade.openstack_cloud().list_volume_backups.return_value = backups
        mock_shade.openstack_cloud().create_volume_backup.return_value = volume_backup1
        mock_shade.openstack_cloud().delete_volume.return_value = mock.Mock()
        mock_shade.openstack_cloud().delete_volume_snapshot.return_value = mock.Mock()
        cloud_config = mock.MagicMock()
        backup_service = BackupService(cloud_config.config, 30)
        backups, failed = backup_service.backup_all(keep_only=1)
        assert len(backups) == 1
        assert len(failed) == 0

    @mock.patch('app.backup.shade')
    def test_backup_all_fails_on_cloud_exception(self, mock_shade):
        """
        Given one backup volume and two volume backups, ensure that backup_all
        completes properly, and returns 0 backups and 1 failure when an
        OpenstackCloudException is encountered
        """

        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'available',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        volume_backup = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'fail_reason': None, 'is_incremental': False,
            'id': 'e8457649-671c-4646-8a8a-5aaf39916e95', 'size': 50}

        def mock_shade_exception(volume_id, force, name, description, wait,
                                 timeout):
            raise OpenStackCloudException('uh-oh cloud exception')

        backup_volumes = [munch.Munch(backup_volume)]
        backups = [munch.Munch(volume_backup)]
        mock_shade.openstack_cloud().list_volumes.return_value = backup_volumes
        mock_shade.openstack_cloud().list_volume_backups.return_value = backups
        mock_shade.openstack_cloud().create_volume_backup.side_effect = mock_shade_exception
        cloud_config = mock.MagicMock()

        backup_service = BackupService(cloud_config.config, 30)
        backups, failed = backup_service.backup_all(keep_only=1)
        assert len(backups) == 0 and len(failed) == 1

    @mock.patch('app.backup.shade')
    def test_backup_all_fails_on_timeout(self, mock_shade):
        """
        Given one backup volume and two volume backups, ensure that backup_all
        completes properly, and returns 0 backups and 1 failure when an
        OpenstackCloudTimeout is encountered
        """

        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'available',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        volume_backup = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'fail_reason': None, 'is_incremental': False,
            'id': 'e8457649-671c-4646-8a8a-5aaf39916e95', 'size': 50}

        def mock_shade_timeout(volume_id, force, name, description, wait,
                               timeout):
            raise OpenStackCloudTimeout('timed out!')

        backup_volumes = [munch.Munch(backup_volume)]
        backups = [munch.Munch(volume_backup)]
        mock_shade.openstack_cloud().list_volumes.return_value = backup_volumes
        mock_shade.openstack_cloud().list_volume_backups.return_value = backups
        mock_shade.openstack_cloud().create_volume_backup.side_effect = mock_shade_timeout
        cloud_config = mock.MagicMock()

        backup_service = BackupService(cloud_config.config, 30)
        backups, failed = backup_service.backup_all(keep_only=1)
        assert len(backups) == 0 and len(failed) == 1

    @mock.patch('app.backup.shade')
    def test_backup_all_fails_on_general_exception(self, mock_shade):
        """
        Given one backup volume and two volume backups, ensure that backup_all
        completes properly, and returns 0 backups and 1 failure when an
        general Exception is encountered
        """

        backup_volume = {
            'id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'name': '',
            'description': '',
            'size': 50,
            'status': 'available',
            'created_at': '2017-08-08T18:29:33.000000',
            'updated_at': '2017-08-08T18:30:01.000000',
            'metadata': {},
            'display_name': '',
            'display_description': '',
            'availability_zone': 'nova',
            'os-vol-tenant-attr:tenant_id': '7b8f9b71e86442a2bf83a63d3ab5b3e4',
            'os-vol-mig-status-attr:migstat': None}
        volume_backup = {
            'status': 'available', 'object_count': 0,
            'container': 'backups',
            'name': 'auto_backup_b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'links': [
                {'href': 'https://my-cloud.example.com:13776'
                         '/v2/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'self'},
                {'href': 'https://my-cloud.example.com:13776'
                         '/7b8f9b71e86442a2bf83a63d3ab5b3e4/backups/'
                         '8fac285e-9608-49a5-90bb-2450d7eff152',
                 'rel': 'bookmark'}
            ],
            'availability_zone': 'nova',
            'created_at': '2018-04-03T03:41:12.000000',
            'description': 'eyJpZCI6ICJhNTNiYzI4YS04OTc2LTQ5N2UtYWMxOC0zYj'
                           'liYzJiMWE2NTgiLCAibmFtZSI6ICIiLCAib3duZXJfdGVu'
                           'YW50X2lkIjogIjdiOGY5YjcxZTg2NDQyYTJiZjgzYTYzZD'
                           'NhYjViM2U0IiwgImRlc2NyaXB0aW9uIjogIiJ9',
            'updated_at': '2018-04-03T03:43:13.000000',
            'data_timestamp': '2018-04-03T03:41:12.000000',
            'has_dependent_backups': False,
            'snapshot_id': None,
            'volume_id': 'b370ea58-41be-47a6-a1e8-36e2af345bd4',
            'fail_reason': None, 'is_incremental': False,
            'id': 'e8457649-671c-4646-8a8a-5aaf39916e95', 'size': 50}


        def mock_general_exception(volume_id, force, name, description, wait,
                                   timeout):
            raise Exception('something bad happened')

        backup_volumes = [munch.Munch(backup_volume)]
        backups = [munch.Munch(volume_backup)]
        mock_shade.openstack_cloud().list_volumes.return_value = backup_volumes
        mock_shade.openstack_cloud().list_volume_backups.return_value = backups
        mock_shade.openstack_cloud().create_volume_backup.side_effect = mock_general_exception
        cloud_config = mock.MagicMock()

        backup_service = BackupService(cloud_config.config, 30)
        backups, failed = backup_service.backup_all(keep_only=1)
        assert len(backups) == 0 and len(failed) == 1
