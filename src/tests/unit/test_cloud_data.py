import os
import ruamel.yaml as yaml

# Local imports
from app.clouds import CloudData


class TestCloudData(object):

    def test_list_clouds(self):
        test_config_path = os.path.join(os.getcwd(),
                                        'tests/files/clouds.yaml')
        c = CloudData(test_config_path)
        clouds = c.list_clouds()

        assert clouds == ['mycloud', 'mycloud2']

    def test_list_clouds_empty(self):
        """
        Ensure we get an empty list if no clouds exist.
        """

        test_config_path = os.path.join('/tmp/clouds.yaml')
        c = CloudData(test_config_path)
        clouds = c.list_clouds()

        assert clouds == []

    def test_cloud_detail(self):
        """
        Ensure we can read details from a clouds.yaml file
        """

        mycloud = {'auth_url': 'https://localhost:13000/v3',
                   'password': 'redacted',
                   'project_name': 'cloud-project',
                   'user_domain_name': 'mydomain',
                   'username': 'myusername'}
        test_config_path = os.path.join(os.getcwd(),
                                        'tests/files/clouds.yaml')
        c = CloudData(test_config_path)
        cloud = c.cloud_detail('mycloud')
        assert cloud == mycloud

    def test_cloud_detail_empty(self):
        """
        Ensure we get an empty response if our cloud does not exist
        """

        test_config_path = os.path.join('/tmp/clouds.yaml')

        c = CloudData(test_config_path)
        cloud = c.cloud_detail('mycloud')
        assert cloud == {}

    def test_create(self):
        """
        Ensure we can create/register a new cloud
        """

        test_config_path = os.path.join('/tmp/test-create.yaml')
        cloud_data = {
            'clouds': {
                'mycloud2': {
                    'auth': {'username': 'newuser',
                             'password': 'password123',
                             'project_name': 'cloud-project',
                             'auth_url': 'https://localhost:13000/v3',
                             'user_domain_name': 'mydomain'}
                }
            }
        }
        c = CloudData(test_config_path)
        status, new_cloud = c.create(cloud_data)

        assert status == 201
        with open(test_config_path, 'r') as cloud_config_file:
            rendered_data = yaml.safe_load(cloud_config_file)
            assert new_cloud == rendered_data

        if os.path.exists(test_config_path):
            os.remove(test_config_path)

    def test_create_negative(self):
        """
        Ensure an exception returns a 500.
        """

        test_config_path = os.path.join('/tmp/test.yaml')
        cloud_data = {'trouble'}

        c = CloudData(test_config_path)
        status, error_message = c.create(cloud_data)

        assert status == 500
        assert error_message == ("New config creation failed 'set' object "
                                 "is not subscriptable")
