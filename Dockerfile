FROM python:3.6-slim

WORKDIR /app

# COPY pip.conf /etc/pip.conf
RUN apt-get update;  apt-get install -y --no-install-recommends autoconf \
                     automake gcc libc6-dev \
                     make; \
                     rm -rf /var/lib/apt/lists/*
RUN mkdir /etc/openstack
COPY src /app
RUN pip3 install -r requirements.txt 

ENTRYPOINT ["/app/docker-entrypoint.sh"]
