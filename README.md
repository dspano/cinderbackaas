# cinderbackaas
CinderBackaaS is a microservice refactor of Gorka Eguilor's cinderback script https://github.com/Akrog/cinderback

It's been refactored to use the Openstack shade library, and is geared for Openstack user's rather than operators.
It talks to an Openstack cloud's Cinder Backup API to create backups. It's main purpose at this time is to be called,
whether manually, or via a cron job to back up all volumes in multiple Openstack projects.

